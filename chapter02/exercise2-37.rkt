#lang racket

(define (dot-product v w)
  (accumulate + 0 (map * v w)))

(define (matrix-*-vector m v)
  (map (lambda (x) (dot-product x v)) m))

(define (transpose m)
  (accumulate-n (lambda (x y) (append x (list y))) '() m))

(define (matrix-*-matrix m n)
  (let ((cols transpose n))
        (map (lambda (ret row) (append ret (map (lambda (col) (dot-product row col)) cols))) m)))
