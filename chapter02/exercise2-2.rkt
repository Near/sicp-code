#lang racket

(define (make-point x y) (cons x y))
(define point-x car)
(define point-y cdr)
(define (make-segment start end) (cons start end))
(define segment-start car)
(define segment-end cdr)

(define (segment-mid seg)
  (make-point
    (/ (+ (point-x (segment-start seg)) (point-x (segment-end seg))) 2)
    (/ (+ (point-y (segment-start seg)) (point-y (segment-end seg))) 2)))

(define (print-point p)
  (display "(")
  (display (point-x p))
  (display ",")
  (display (point-y p))
  (display ")")
  (newline))

(define orig (make-point 0 0))
(print-point orig)
(define unit (make-point 1 1))
(print-point unit)
(define seg (make-segment orig unit))
(print-point (segment-mid seg))
