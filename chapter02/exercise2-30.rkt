#lang racket

(define (square-tree tree)
  (map (lambda (x)
    (if (pair? x)
        (square-tree x)
        (* x x))) tree))

(square-tree '(1 (2 (3 4) 5) (6 7)))
