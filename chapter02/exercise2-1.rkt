#lang racket

(define (gcd a b)
  (if (< a b)
      (gcd b a)
      (if (= b 0)
          a
          (gcd b (remainder a b)))))

(define (make-rat a b)
  (let ((g (gcd (abs a) (abs b))))
    (cond ((>= (* a b) 0) (cons (/ (abs a) g) (/ (abs b) g)))
          (else (cons (* -1 (/ (abs a) g)) (/ (abs b) g))))))

(define numer car)
(define denom cdr)

(define (add-rat x y)
  (make-rat (+ (* (numer x) (denom y)) (* (numer y) (denom x)))
            (* (denom x) (denom y))))

(define (sub-rat x y)
  (make-rat (- (* (numer x) (denom y))
               (* (numer y) (denom x)))
            (* (denom x) (denom y))))

(define (mul-rat x y)
  (make-rat (* (numer x) (numer y)) (* (denom x) (denom y))))

(define (div-rat x y)
    (make-rat (* (numer x) (denom y)) (* (denom x) (numer y))))

(define (equal-rat? x y)
  (= (* (numer x) (denom y)) (* (denom x) (numer y))))

(define (print-rat x)
  (display (numer x))
  (display "/")
  (display (denom x))
  (newline))

(define neg-half (make-rat 2 -4))
(define half (make-rat 2 4))
(print-rat neg-half)
(print-rat (add-rat neg-half half))
