#lang racket

(define (same-parity x . l)
  (define (same-parity-recur ll)
    (cond ((null? ll) ll)
          ((= (remainder x 2) (remainder (car ll) 2)) (cons (car ll) (same-parity-recur (cdr ll))))
          (else (same-parity-recur (cdr ll)))))
  (cons x (same-parity-recur l)))

(same-parity 2 3 4 5 6 7)
