#lang racket

(define (cont-frac fn fd counter)
  (define (iter result counter)
    (if (= counter 0)
        result
        (iter (/ (fn counter) (+ (fd counter) result)) (- counter 1))
    ))
  (iter 0 counter))

(define (approximate-e k)
  (+ 2 (cont-frac
    (lambda (x) 1.0)
    (lambda (x)
      (let ((r (remainder (+ x 1) 3))
            (d (* 2 (/ (+ x 1) 3))))
          (if (= r 0) d 1)
      ))
    k)))

(approximate-e 100)
