#lang racket

(define (repeated f n)
  (define (iter counter) (lambda (x)
    (if (= counter 1)
        (f x)
        (f ((iter (- counter 1)) x)))))
  (iter n))

((repeated (lambda (x) (* x x)) 2) 5)
