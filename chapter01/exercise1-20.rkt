#lang racket

(define (gcd a b)
  (if (< a b)
      (gcd b a)
      (if (= b 0)
          a
          (gcd b (remainder a b)))))

; regular order
(gcd 206 40)
; (gcd 40 (remainder 206 40)) ; 1 remainder
; (gcd (remainder 206 40) (remainder 40 (remainder 206 40))) ; 2 remainder
; (gcd (remainder 40 (remainder 206 40)) (remainder (remainder 206 40) (remainder 40 (remainder 206 40)))) ; 4 remainder
; (gcd (remainder (remainder 206 40) (remainder 40 (remainder 206 40))) (remainder (remainder 40 (remainder 206 40)) (remainder (remainder 206 40) (remainder 40 (remainder 206 40)))) ; 7 remainder
; (remainder (remainder 206 40) (remainder 40 (remainder 206 40))) ; 4 remainder
; 2
; ; 18 remainder

; application order
(gcd 206 40)
; (gcd 40 (remainder 206 40)) ; 1 remainder
; (gcd 40 6)
; (gcd 6 (remainder 40 6)) ; 1 remainder
; (gcd 6 4)
; (gcd 4 (remainder 6 4)) ; 1 remainder
; (gcd 4 2)
; (gcd 2 (remainde 4 2)) ; 1 remainder
; (gcd 2 0)
; 2
; ; 4 remainder
