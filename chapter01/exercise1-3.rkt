#lang racket

(define (sum-of-greater-two-of-three a b c)
  (cond ((and (> a c) (> b c)) (+ a b))
        ((and (> a b) (> c b)) (+ a c))
        (else (+ b c))))

(sum-of-greater-two-of-three 1 1 1)
(sum-of-greater-two-of-three 1 1 2)
(sum-of-greater-two-of-three 1 2 3)
