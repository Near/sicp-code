#lang racket

(define tolerence 0.00001)

(define (fixed-point f first-guess)
  (define (close-enough? x y) (< (abs (- x y)) tolerence))
  (define (try guess) (begin
      (display guess)
      (newline)
      (let ((next (f guess)))
        (cond ((close-enough? next guess) next)
              (else (try next))))))
  (try first-guess))

(define (average-damp f) (lambda (x) (/ (+ (f x) x) 2.0)))

(define (repeated f n)
  (define (iter counter) (lambda (x)
    (if (= counter 0)
        x
        (f ((iter (- counter 1)) x)))))
  (iter n))

(define (pow n) (lambda (base) (if (= n 0) 1 ((repeated (lambda (x) (* x base)) (- n 1)) base))))

(define (average-n-damp f n) ((repeated average-damp n) f))

(define times 2)

(define (nrt n x) (fixed-point (average-n-damp (lambda (y) (/ x ((pow (- n 1)) y))) times) 1.0))

(nrt 4 4.0)
