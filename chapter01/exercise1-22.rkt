#lang racket

(define (smallest-dividor n)
  (define (iter n test)
    (if (> (* test test) n)
        n
        (if (= (remainder n test) 0)
            test
            (iter n (+ test 1))
        )))
  (iter n 2)
)

(define (prime? n) (= (smallest-dividor n) n))

(define (time-prime-test n)
  (display n)
  (start-prime-test n (current-inexact-milliseconds)))

(define (start-prime-test n start-time)
  (if (prime? n)
      (report-prime (- (current-inexact-milliseconds) start-time))
      (newline)))

(define (report-prime elapsed-time)
  (display " time used is : ")
  (display elapsed-time)
  (newline))

(define (search-for-primes n counter)
  (define (next-num x)
    (if (= x 2) (+ x 1) (+ x 2)))
  (define (iter n counter start-time)
    (if (= counter 0)
        (report-prime (- (current-inexact-milliseconds) start-time))
        (if (prime? n)
            (iter (next-num n) (- counter 1) start-time)
            (iter (next-num n) counter start-time))))
  (if (and (even? n) (> n 2))
      (iter (+ n 1) counter (current-inexact-milliseconds))
      (iter n counter (current-inexact-milliseconds))))

(time-prime-test 1009)
(time-prime-test 10007)
(time-prime-test 100003)
(time-prime-test 1000003)
(search-for-primes 1000 3)
(search-for-primes 10000 3)
(search-for-primes 100000 3)
(search-for-primes 1000000 3)
(search-for-primes 10000000 3)
(search-for-primes 100000000 3)
