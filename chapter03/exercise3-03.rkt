#lang racket

(define (make-account b p)
  (let ((balance b)
        (password p)
        (counter 0))
        (define (withdraw amount)
          (if (< amount balance)
              (begin
                (set! balance (- balance amount))
                balance)
              (display "Balance not enough\n")))
        (define (deposit amount)
          (if (> amount 0)
              (begin
                (set! balance (+ balance amount))
                balance)
              (display "Deposit amount negative\n")))
        (define (dispatch pass cmd)
          (if (not (eq? pass password))
              (begin
                (set! counter (+ counter 1))
                (if (>= counter 7)
                    (display "call-police\n")
                    (display "Incorrect password\n")))
              (begin
                (set! counter 0)
                (cond ((eq? cmd 'withdraw) withdraw)
                      ((eq? cmd 'deposit) deposit)
                      (else (display "Unknown request\n"))))))
        dispatch))

(define acc (make-account 100 'hahahaha))

((acc 'hahahaha 'withdraw) 40)
(acc 'Hahahaha 'deposit)
(acc 'Hahahaha 'deposit)
(acc 'Hahahaha 'deposit)
(acc 'Hahahaha 'deposit)
(acc 'Hahahaha 'deposit)
(acc 'Hahahaha 'deposit)
(acc 'Hahahaha 'deposit)
(acc 'Hahahaha 'deposit)
(acc 'Hahahaha 'deposit)
