#lang racket

(define (make-accumulator num)
  (lambda (inc)
    (begin (set! num (+ num inc)) num)))

(define A (make-accumulator 5))

(A 10)

(A 10)
